package com.axeles.swoosh.Model

import android.os.Parcel
import android.os.Parcelable

class Player constructor(var league: String, var skill: String): Parcelable {

  constructor(parcel: Parcel) : this(
    parcel.readString(),
    parcel.readString()
  ) {}

  override fun describeContents(): Int {
    return 0
  }

  override fun writeToParcel(dest: Parcel?, flags: Int) {
    dest?.writeString(league)
    dest?.writeString(skill)
  }

  companion object CREATOR : Parcelable.Creator<Player> {
    override fun createFromParcel(parcel: Parcel): Player {
      return Player(parcel)
    }

    override fun newArray(size: Int): Array<Player?> {
      return arrayOfNulls(size)
    }
  }
}