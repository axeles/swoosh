package com.axeles.swoosh.Controllers

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.Toast
import com.axeles.swoosh.Model.Player
import com.axeles.swoosh.Utilities.EXTRA_PLAYER
import com.axeles.swoosh.R
import kotlinx.android.synthetic.main.activity_skill.*
import kotlin.math.E

class SkillActivity : BaseActivity() {

  lateinit var player: Player

  override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
    super.onSaveInstanceState(outState, outPersistentState)
    outState?.putParcelable(EXTRA_PLAYER, player)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_skill)
    player = intent.getParcelableExtra<Player>(EXTRA_PLAYER)
  }

  override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
    super.onRestoreInstanceState(savedInstanceState)
    if(savedInstanceState != null) {
      player = savedInstanceState.getParcelable(EXTRA_PLAYER)
    }
  }

  fun onBeginnerSkillClicked(view: View) {
    ballerSkillBtn.isChecked = false

    player.skill = "beginner"
  }

  fun onBallerSkillClicked(view: View) {
    beginnerSkillBtn.isChecked = false

    player.skill = "baller"
  }

  fun onSkillFinishClicked(view: View) {
    if(player.skill != "") {
      val finalActivity = Intent(this, FinalActivity::class.java)
      finalActivity.putExtra(EXTRA_PLAYER, player)
      startActivity(finalActivity)
    } else {
      Toast.makeText(this, "Please select a skill level.", Toast.LENGTH_SHORT).show()
    }
  }

}
