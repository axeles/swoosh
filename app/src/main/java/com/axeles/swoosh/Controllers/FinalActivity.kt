package com.axeles.swoosh.Controllers

import android.os.Bundle
import com.axeles.swoosh.Model.Player
import com.axeles.swoosh.R
import com.axeles.swoosh.Utilities.EXTRA_PLAYER
import kotlinx.android.synthetic.main.activity_final.*

class FinalActivity : BaseActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_final)

    val player = intent.getParcelableExtra<Player>(EXTRA_PLAYER)

    searchLeaguesText.text = "Looking for ${player.league} ${player.skill} league near you..."
  }
}
